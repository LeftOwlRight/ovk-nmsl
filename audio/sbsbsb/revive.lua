if not OVKNMSL_PD3 then
    dofile(ModPath .. "menu.lua")
end

if not OVKNMSL_PD3.settings.other_meme.SBSBSB.SB_enable then
    return
end


Hooks:PostHook(ReviveInteractionExt, "_at_interact_start", "_at_interact_start_OVKNMSL_PD3", function(self, player, timer)
    local played_path, sb_source = OVKNMSL_PD3:play_audio(OVKNMSL_PD3.settings.other_meme.SBSBSB.SB_self_volume, "player", "sbsbsb", true)

    OVKNMSL_PD3.sbsbsb_source = sb_source
    --[[for k,v in ipairs(OVKNMSL_PD3.sbsbsb_source) do
        managers.mission._fading_debug_output:script().log("k is "..k.."v is "..v, Color.white)
    end--]]

    local SB_message = {
        key_1 = "played_path",
        value_1 = played_path
    }
    if played_path then
        LuaNetworking:SendToPeers('SBSBSB_sent_my_id', json.encode(SB_message))
    end
end)



Hooks:PostHook(ReviveInteractionExt, "_at_interact_interupt", "_at_interact_interupt_OVKNMSL_PD3", function(self, player, complete)
    if OVKNMSL_PD3.sbsbsb_source and OVKNMSL_PD3.sbsbsb_source.stop and not OVKNMSL_PD3.sbsbsb_source:is_closed() then
        local SB_message = {
            key_1 = "1",
            value_1 = "2"
        }
        OVKNMSL_PD3.sbsbsb_source:stop()
        LuaNetworking:SendToPeers('SBSBSB_stop', json.encode(SB_message))
    end
end)
