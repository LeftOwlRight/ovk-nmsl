--localization

local mpath=ModPath

Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_OVKNMSL_PD3", function(loc)
	local path="loc/schinese.txt"
	loc:load_localization_file(mpath..path)
end)

