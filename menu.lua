OVKNMSL_PD3 = OVKNMSL_PD3 or class()

OVKNMSL_PD3.settings = {
    enable = true,
    self_volume = 0.65,
    others_volume = 0.40,
    other_meme = {
        kfc_did_it = {
            kfc_enable = true,
            kfc_self_volume = 0.65,
            kfc_others_volume = 0.40,
            lr_self_volume = 1.0,
            lr_others_volume = 0.75,
            rg_self_volume = 0.9,
            rg_others_volume = 0.65,
            cb_self_volume = 1.35,
            cb_others_volume = 0.9,
            vn_self_volume = 1.75,
            vn_others_volume = 1.25
        },
        SBSBSB = {
            SB_enable = false,
            SB_sync = true,
            SB_self_volume = 0.65,
            SB_others_volume = 0.40
        }
    }
}

OVKNMSL_PD3.values = {
    kfc_did_it = {
        priority = 80
    },
    SBSBSB = {
        priority = 50
    },
    SB_enable = {
        priority = 80
    },
    SB_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 60
    },
    SB_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 50
    },
    enable = {
        priority = 80
    },
    self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 60
    },
    others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 50
    },

    kfc_enable = {
        priority = 80
    },
    kfc_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 60
    },
    kfc_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 50
    },
    rg_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 40
    },
    rg_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 30
    },
    cb_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 25
    },
    cb_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 20
    },
    vn_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 15
    },
    vn_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = 10
    },
    lr_self_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = -20
    },
    lr_others_volume = {
        min = 0,
        max = 2,
        step = 0.01,
        priority = -30
    }
}

OVKNMSL_PD3.modpath = ModPath

local builder = MenuBuilder:new("ovk_no_mama", OVKNMSL_PD3.settings, OVKNMSL_PD3.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusOVKNMSL_PD3", function(menu_manager, nodes)
    builder:create_menu(nodes)
end)

-- localization
dofile(OVKNMSL_PD3.modpath .. "localization.lua")

function OVKNMSL_PD3:play_audio(volume, unit, which, default, path)
    local played_volume = volume

    local function random_sbsbsb()
        local playvolume = played_volume or volume or 0.65
        local oggpath = OVKNMSL_PD3.modpath .. "audio/sbsbsb/SB_Fast.ogg"
        local rognum = math.random(1, 100)
        if rognum < 90 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/sbsbsb/SB_Normal.ogg"
        end
        if not default then
            playvolume = played_volume or volume
        end
        return oggpath, playvolume
    end

    local function random_ovknmsl()
        local playvolume = played_volume or volume or 0.65
        local oggpath = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die.ogg"
        local rognum = math.random(1, 100)
        if rognum < 15 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die11.ogg"
        elseif rognum < 40 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die10.ogg"
        elseif rognum < 70 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die2.ogg"
        end
        if not default then
            playvolume = played_volume or volume
        end
        return oggpath, playvolume
    end

    local function random_kfcdidit()
        local playvolume = played_volume or volume or 0.65
        local oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/kfc3.ogg"
        local rognum = math.random(1, 135)
        if rognum < 25 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/kfc4.ogg"
        elseif rognum < 45 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/rg6.ogg"
            playvolume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.rg_self_volume
        elseif rognum < 65 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/rg7.ogg"
            playvolume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.rg_self_volume
        elseif rognum < 75 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/lr5.ogg"
            playvolume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.lr_self_volume
        elseif rognum < 90 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/cb8.ogg"
            playvolume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.cb_self_volume
        elseif rognum < 110 then
            oggpath = OVKNMSL_PD3.modpath .. "audio/kfcdidit/vn9.ogg"
            playvolume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.vn_self_volume
        end
        if not default then
            playvolume = played_volume or volume
        end
        return oggpath, playvolume
    end

    local which_function
    if which == "ovknmsl" then
        which_function, played_volume = random_ovknmsl()
    elseif which == "kfcdidit" then
        which_function, played_volume = random_kfcdidit()
    elseif which == "sbsbsb" then
        which_function, played_volume = random_sbsbsb()
    else
        which_function, played_volume = random_ovknmsl()
    end

    local unit_play
    local unit_type = type(unit)
    if unit_type == "string" and (unit == "player" or unit == "Player" or unit == "PLAYER") then
        unit_play = XAudio.PLAYER
    else
        unit_play = unit
    end

    local ogg_path = ""
    if unit then
        if unit_type == "userdata" then
            if not alive(unit) then
                return
            end
        end
        blt.xaudio.setup()

        if default then
            ogg_path = which_function
        elseif path then
            ogg_path = path
        end

        local source = XAudio.UnitSource:new(unit_play, XAudio.Buffer:new(ogg_path))
        source:set_volume(played_volume)
        return ogg_path, source
    else
        blt.xaudio.setup()
        if default then
            ogg_path = which_function
        else
            ogg_path = path
        end
        local source = XAudio.Source:new(XAudio.Buffer:new(ogg_path))
        source:set_volume(played_volume)
        return ogg_path, source
    end
end


local function OVKNMSL_get_PeerUnit_by_PeerID(peer_id)
    local peer = managers.network:session():peer(peer_id)
    if peer then
        return peer:unit()
    end
end

-- LuaNetworking Receiver
Hooks:Add("NetworkReceivedData", "NetworkReceivedData_OVKNMSL_PD3", function(sender, id, data)
    local peer_unit = OVKNMSL_get_PeerUnit_by_PeerID(sender)
    local table_get_from_data = {}
    if data then
        table_get_from_data = json.decode(data)
    end
    if id == "OVKNMSL_sent_my_id" then
        if not OVKNMSL_PD3.settings.enable then
            return
        end
        if table_get_from_data and table_get_from_data.value_1 then
            local preplay_path = table_get_from_data.value_1
            local last_character = string.sub(preplay_path, -6)
            local volume = OVKNMSL_PD3.settings.others_volume

            if last_character == "e2.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die2.ogg"
            elseif last_character == "ie.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die.ogg"
            elseif last_character == "10.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die10.ogg"
            elseif last_character == "11.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/ovknmsl/die11.ogg"
            end

            if io.file_is_readable(preplay_path) then
                OVKNMSL_PD3:play_audio(volume, peer_unit, "ovknmsl", false, preplay_path)
            end
            
        end
    elseif id == "KFC_sent_my_id" then
        if not OVKNMSL_PD3.settings.other_meme.kfc_did_it.kfc_enable then
            return
        end
        if table_get_from_data and table_get_from_data.value_1 then
            local preplay_path = table_get_from_data.value_1
            local last_character = string.sub(preplay_path, -5)
            local volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.kfc_others_volume or 0.35

            if last_character == "3.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/kfc3.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.kfc_others_volume or 0.35
            elseif last_character == "4.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/kfc4.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.kfc_others_volume or 0.35
            elseif last_character == "5.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/lr5.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.lr_others_volume
            elseif last_character == "6.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/rg6.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.rg_others_volume
            elseif last_character == "7.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/rg7.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.rg_others_volume
            elseif last_character == "8.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/cb8.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.cb_others_volume
            elseif last_character == "9.ogg" then
                preplay_path = OVKNMSL_PD3.modpath .. "audio/kfcdidit/vn9.ogg"
                volume = OVKNMSL_PD3.settings.other_meme.kfc_did_it.vn_others_volume
            end

            if io.file_is_readable(preplay_path) then
                OVKNMSL_PD3:play_audio(volume, peer_unit, "kfcdidit", false, preplay_path)
            end
            
        end
    elseif id == "SBSBSB_sent_my_id" then
        if not OVKNMSL_PD3.settings.other_meme.SBSBSB.SB_sync then
            return
        end
        local preplay_path = table_get_from_data.value_1
        local last_character = string.sub(preplay_path, -5)
        local volume = OVKNMSL_PD3.settings.other_meme.SBSBSB.SB_others_volume or 0.35

        if last_character == "t.ogg" then
            preplay_path = OVKNMSL_PD3.modpath .. "audio/sbsbsb/SB_Fast.ogg"
        elseif last_character == "l.ogg" then
            preplay_path = OVKNMSL_PD3.modpath .. "audio/sbsbsb/SB_Normal.ogg"
        end

        if io.file_is_readable(preplay_path) then
            local played_path, sb_source = OVKNMSL_PD3:play_audio(OVKNMSL_PD3.settings.other_meme.SBSBSB.SB_self_volume, "player", "sbsbsb", true)
            OVKNMSL_PD3.sbsbsb_source_stop = sb_source
        end
    elseif id == "SBSBSB_stop" then
        if OVKNMSL_PD3.sbsbsb_source_stop and OVKNMSL_PD3.sbsbsb_source_stop.stop and not OVKNMSL_PD3.sbsbsb_source_stop:is_closed() then
            OVKNMSL_PD3.sbsbsb_source_stop:stop()
        end
    end
end)

